# Dokumentasi Project

## Instalasi 

1. Download seluruh file kemudian ekstrak
2. Buka terminal install semua package yang dibutuhkan aplikasi :

    `npm install`


## Menjalankan Aplikasi

1. Run json-server-dev
    
    `npm run dev:json`

2. Masuk ke folde jwt-auth kemudian run program
    
    `php -S localhost:8080`

3. Kembali ke folde root aplikasi, kemudian run angular app dengan mode build
    
    `npm run dev:start`

## Login

User Login default :

```
    email : test@admin.com
    password : admin
```