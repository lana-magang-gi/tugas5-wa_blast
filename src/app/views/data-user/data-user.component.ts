import { Component, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { DataUserService } from 'src/app/utils/services/data-user/data-user.service';
import {NgbModal, } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { ModalComponent } from './modal/modal.component';

@Component({
  selector: 'app-data-user',
  templateUrl: './data-user.component.html',
  styleUrls: ['./data-user.component.scss']
})
export class DataUserComponent implements OnInit, AfterViewInit {
  public dtOptions: DataTables.Settings = {};
  public _data;
  public _formGroup: FormGroup;

  constructor(
    private renderer: Renderer2,
    private userService: DataUserService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder
  ) {
    this._formGroup = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {
    this.getDataWithAjax();
  }

  ngAfterViewInit(): void {
    this.renderer.listen('document', 'click', event => {
      if (event.target.hasAttribute('data-edit')) {
        this.modalEdit(event.target.getAttribute('data-edit'));
      } else if (event.target.hasAttribute('data-hapus')){
        this.modalDelete(event.target.getAttribute('data-hapus'));
      }
    })
  }

  getDataWithAjax(): void {
    let index = 1;

    this.dtOptions = {
      ajax: {
        url: `https://my-json-server.typicode.com/lana404/Tugas5-WABlast/user`,
        dataSrc: ''
      },
      responsive: true,
      serverSide: false,
      processing: true,
      columnDefs: [{ 
        targets: 0, 
        render: () => {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
          return index++;
        }, 
        orderable: false, 
        searchable: false
      }, { 
        targets: 1, 
        data: 'id'
      }, { 
        targets: 2, 
        data: 'name'
      }, { 
        targets: 3, 
        data: 'email'
      }, { 
        targets: 4, 
        data: 'password', 
        orderable: false, 
        searchable: false
      }, { 
        targets: 5, 
        data: 'id',
        render:  (data:any, type:any, full:any)=> {
          return `
            <button type="button" class="btn btn-success" data-edit="${data}"> Edit </button>
            &nbsp;
            <button type="button" class="btn btn-danger" data-hapus="${data}"> Hapus </button>
          `;
        }, 
        orderable: false, 
        searchable: false
      }],
    }
  }

  modalDelete(id: number): void{
    let modalPage = this.modalService.open(ModalComponent);
    modalPage.componentInstance._id = id;
    modalPage.componentInstance._hapus = true;
  }

  modalEdit(id = null) {
    let modalPage = this.modalService.open(ModalComponent);
    modalPage.componentInstance._id = id
  }
}