import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataUserService } from '../../../utils/services/data-user/data-user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  public _formGroup: FormGroup;
  public formVisible = false;
  public _data:any;
  @Input()
    public _id: number;
    public _hapus = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: DataUserService,
    private modalService: NgbModal,
    private toastr: ToastrService,
  ) { 
    this._formGroup = this.formBuilder.group({
      id: new FormControl(null, [Validators.required]),
      name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
  }

  ngOnInit(): void {
    if (this._id !== null && this._hapus !== true) {
      this.userService.getData(this._id).subscribe(
        data => {
          this._data = data;
          this._formGroup.setValue({
            id: this._data.id,
            name: this._data.name,
            email: this._data.email,
            password: this._data.password
          })
        },
        () => {
          console.log('Error');
        }
      );
    }
    this._formGroup.get('id').disable();
    console.log(this._hapus);
    
  }

  modalClose() {
    this.modalService.dismissAll();
  }

  tambahData() {
    this._data = this._formGroup.value;

    if (this._formGroup.valid) {
      this.userService.tambahData(this._data).subscribe(
        () => {
          console.log('Gagal Menambahkan Data');
        },
        () => {
          console.error('Gagal Menambahkan Data');
        }
      )
    } else {
      if (this._formGroup.get('name').invalid) {
        this.toastr.error('Nama tak boleh kosong', 'Validasi');
      } else if (this._formGroup.get('email').invalid){
        this.toastr.error('Format Email Salah', 'Validasi');
      } else if (this._formGroup.get('password').invalid) {
        this.toastr.error('Password tak boleh kosong', 'Validasi');
      }
    }
  }

  editData() {
    this._data = this._formGroup.value;

    if (this._formGroup.valid) {
      this.userService.updateData(this._data, this._id).subscribe(
        () => {
          console.log('Gagal Menambahkan Data');
        },
        () => {
          console.error('Gagal Menambahkan Data');
        }
      );
    } else {
      if (this._formGroup.get('name').invalid) {
        this.toastr.error('Nama tak boleh kosong', 'Validasi');
      } else if (this._formGroup.get('email').invalid){
        this.toastr.error('Format Email Salah', 'Validasi');
      } else if (this._formGroup.get('password').invalid) {
        this.toastr.error('Password tak boleh kosong', 'Validasi');
      }
    }
  }

  hapusData() {
    this.userService.hapusData(this._id).subscribe(
      () => {
        console.log('Sukses Menghapus Data');
      },
      () => {
        console.error('Gagal Menghapus Data');
      }
    )
  }
}
