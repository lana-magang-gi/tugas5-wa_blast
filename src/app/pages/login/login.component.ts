import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { AppService } from '../../utils/services/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public isAuthLoading = false;
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private appService: AppService,
    private router: Router
  ) {}

  ngOnInit() {
    this.renderer.addClass(document.querySelector('app-root'), 'login-page');
    this.loginForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  login() {
    let data = this.loginForm.value;

    if (!this.loginForm.get('email').valid) {
      this.toastr.error('Harap Masukan Email!!!', 'Validasi');
    } else if(!this.loginForm.get('password').valid){
      this.toastr.error('Harap Masukan Password!!!', 'Validasi');
    } else if(this.loginForm.valid) {
      this.appService.login(data).subscribe(
        () => {
          console.log('Sukses Login');
          this.router.navigate(['/']);
        },
        () => {
          this.toastr.error('Harap Masukan Email dan Password yang benar', 'Login Gagal');
        }
      )
    } else {
      this.toastr.error('Something Wrong', 'Login Gagal');
    }
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.querySelector('app-root'), 'login-page');
  }
}
