import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataUser } from './data-user';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataUserService {
  //private baseUrl = 'http://localhost:3030/user';
  private baseUrl = 'https://my-json-server.typicode.com/lana404/Tugas5-WABlast/user';
  
  constructor(
    private httpClient: HttpClient
  ) { }

  handleError(err:any) {
    console.log(err);
    return throwError(err);
  }

  hapusData(id:number) {
    return this.httpClient.delete(`${this.baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  getData(id:number) {
    return this.httpClient.get(`${this.baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    )
  }

  updateData(data:any, id:number) {
    let searchParam = new URLSearchParams();
    searchParam.set('name', data.name);
    searchParam.set('email', data.email);
    searchParam.set('password', data.password);

    let option = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }

    return this.httpClient.put(`${this.baseUrl}/${id}`, searchParam.toString(), option).pipe(
      catchError(this.handleError)
    )
  }

  tambahData(data:any) {
    return this.httpClient.post(`${this.baseUrl}`, data).pipe(
      catchError(this.handleError)
    )
  }
}
