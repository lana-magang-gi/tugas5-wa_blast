import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private baseUrl = location.origin;
  public user = {

  };

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    
  ) {}

  handleError(err:any) {
    console.log(err);
    return throwError(err);
  }

  login(data: { email: string; password: string; }) {
    //localStorage.setItem('token', 'LOGGED_IN');
    let searchParam = new URLSearchParams();
    searchParam.set('email', data.email);
    searchParam.set('password', data.password);

    let option = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }

    return this.httpClient.post(`${this.baseUrl}/ihwan/jwt-auth/index.php`, searchParam.toString(), option).do( resp => this.setSession(resp)).shareReplay();
  }

  private setSession(authResult:any) {
    localStorage.setItem('token', authResult.token);
  }  

  register() {
    localStorage.setItem('token', 'LOGGED_IN');
    this.router.navigate(['/']);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
}
