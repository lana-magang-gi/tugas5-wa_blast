<?php
require 'jwt_helper.php';
header("Access-Control-Allow-Origin: *");

function authenticate () {
    $db_usr = "test@admin.com";
    $db_usr_pw = "admin";
    $db_usr_id = 187;
    $secret_key = 'test_key';
    $valid_for = '3600';
    if ($_POST['email'] && $_POST['password']) {
        $usr = $_POST['email'];
        $pw = $_POST['password'];
        if ($usr == $db_usr && $pw == $db_usr_pw) {
            $token = array();
            $token['id'] = $db_usr_id;
            $token['exp'] = time() + $valid_for;
            echo json_encode(array('token' => JWT::encode($token, $secret_key)));
            return false;
        } else {
            http_response_code(401);
            echo "Auth Gagal";
            return false;
        }
    } else {
        $headers = getallheaders();
        if (array_key_exists('Authorization', $headers)) {
            $jwt = $headers['Authorization'];
            $token = JWT::decode($jwt, $secret_key);
            if ($token->exp >= time()) {
                //loggedin
                return $token->id;
            } else {
                http_response_code(401);
                return false;
            }
        } else {
            http_response_code(401);
            return false;
        }
    }
}
?>